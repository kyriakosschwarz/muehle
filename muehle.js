//    0---1---2---3---4---5---6
//    |           |           |
//    1   1---2---3---4---5   |
//    |   |       |       |   |
//    2   2   2---3---4   |   |
//    |   |   |       |   |   |
//    3---3---3       |---|---|
//    |   |   |       |   |   |
//    4   4   4-------+   |   |
//    |   |       |       |   |
//    5   5---------------+   |
//    |           |           |
//    6-----------------------+

var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
ctx.font = "30px Arial";

var nrOfStones = 9;
var nearPosDist = 30;

var size = 540;
var unit = size / 6;
var startpos = (canvas.height - size) / 2;

var newStoneX = 800;
var newStoneY = startpos;
var newStoneR = 20;

var clickState = {
	nearPos: null,
	oldStone: null,
	currentStone: null,
	downInStone: false
};

var gameState = {
	moveOnBoard: false,
	removeStone: false,
	positions: [],
	players: [],
	currentPlayer: null
};

function Player(color) {
	this.color = color;
	this.stones = [];
	this.stonesPlaced = 0;
}

Player.prototype.addStone = function(stone) {
	stone.fill = this.color;
	if (this.color == 'black') stone.y += 100;
	this.stones.push(stone);
};

Player.prototype.removeStone = function(stone) {
	var index = this.stones.findIndex(s => equalPosition(s, stone));
	this.stones.splice(index, 1);
};

Player.prototype.drawStones = function() {
	for (var i = 0, len = this.stones.length; i < len; i++) {
		this.stones[i].draw();
	}
};

Player.prototype.canMove = function() {
	if (this.canJump()) return true;
	for (var i = 0; i < this.stones.length; i++) {
		var neighPos = neighborPos(this.stones[i]);
		for (var j = 0; j < neighPos.length; j++) {
			if (!neighPos[j].occupied()) return true;
		}
	}
	return false;
};

Player.prototype.canJump = function() {
	return !gameState.moveOnBoard || this.stones.length == 3;
};

Player.prototype.hasTooFew = function() {
	return gameState.moveOnBoard && this.stones.length < 3;
};

function Stone(x, y) {
	this.x = x;
	this.y = y;
	this.r = newStoneR;
	this.fill = 'undefined color';
	this.firstMove = true;
}

Stone.prototype.draw = function() {
  	filledCircle(this.x, this.y, this.r, this.fill);
	emptyCircle(this.x, this.y, this.r, 'black');
};

Stone.prototype.contains = function(mx, my) {
	return distance(this, {x: mx, y: my}) < this.r;
};

Stone.prototype.isInMill = function() {
	if (this.firstMove) return false;
	var hor = 0, ver = 0;
	for (var i = 0, len = gameState.positions.length; i < len; i++) {
		var pos = gameState.positions[i];
		if (pos.occupied() && sameColor(this, pos.cargo) && !isAcrossCenter(this, pos, true)) {
			if (this.y == pos.y) hor++;
			if (this.x == pos.x) ver++;
		}
	}
	return hor >= 3 || ver >= 3;
};

function Position (x, y) {
	this.x = x;
	this.y = y;
	this.name = x + ',' + y;
	this.cargo = null; 			// holds a stone
}

Position.prototype.draw = function() {
	filledCircle(this.x, this.y, 5, '#333');
};

Position.prototype.occupied = function() {
	return this.cargo !== null;
};

Position.prototype.contains = function(mx, my) {
	return distance(this, {x: mx, y: my}) < nearPosDist;
};	

init();

canvas.onmousedown = mouseDown;
canvas.onmouseup = mouseUp;

function init() {
	initPositions();
	initPlayers();
	return setInterval(draw, 10);
}

function draw() {
	clear();
	drawBoard();
	drawPlayerStones();
	drawRemainingNr();
}

function clear() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function mouseDown(e) {
	if (clickInStone(e)) {
		clickState.downInStone = true;
		clickState.oldStone = {x: clickState.currentStone.x, y: clickState.currentStone.y};
		if (!gameState.removeStone) canvas.onmousemove = mouseMove;
	}
}

function mouseMove(e){
	updateCoords({x: e.pageX, y: e.pageY});
}

function mouseUp(e) {
	if (clickState.downInStone) handleGameRules(e);
	clearClick();
}

function handleGameRules(e) {

		// cannot remove stone that is in mill, except if all stones are in mill
		var checkRemoveInMill = (clickState.currentStone.isInMill())? !allBoardStonesInMill() : false;

		// stone can jump if not all stones are on board, or if there are only 3 stones on board
		// otherwise stone can move only to neighbor positions
		var checkNeighbor = ((gameState.currentPlayer.canJump())? true : inNeighborPos(e));

		// cannot remove stones that are not on board
		var removeAllowed = gameState.removeStone && !(clickState.currentStone.firstMove || checkRemoveInMill);

		// cannot move stones that are on board, except if all stones are on board
		var moveAllowed = (clickState.currentStone.firstMove || gameState.moveOnBoard) && 
						  upNearUnoccupiedPos(e) && checkNeighbor && !gameState.removeStone;

		if (removeAllowed) {

			freeOldPos(clickState.oldStone);
			gameState.currentPlayer.removeStone(clickState.currentStone);
			gameState.removeStone = false;
			
		} else if (moveAllowed) {

			moveToNewPos();
			freeOldPos(clickState.oldStone);
			clickState.nearPos.cargo = clickState.currentStone;
			pushNewStone();
			checkMoveOnBoard();
			checkIfRemove(clickState.currentStone);
			nextPlayerToMove();
		
		} else {

			moveToOldPos();
		}

		checkIfWin();
}

function clearClick() {	
	canvas.onmousemove = null;
	clickState.nearPos = null;
	clickState.oldStone = null;
	clickState.currentStone = null;
	clickState.downInStone = false;
}

function checkIfWin() {
	if (gameState.currentPlayer.hasTooFew() || !gameState.currentPlayer.canMove()) {
		alert("YOU WIN, CONGRATS!");
    	document.location.reload();	
	}	
}

function checkIfRemove(stone) {
	if (stone.isInMill()) {
		gameState.removeStone = true;
		console.log('removeStone!');
	}
}

function checkMoveOnBoard() {
	var sum = 0;
	for (var i = 0, len = gameState.players.length; i < len; i++) {
		sum += gameState.players[i].stonesPlaced;
	}
	if (sum == nrOfStones * gameState.players.length) gameState.moveOnBoard = true;
}

function moveToOldPos() {
	updateCoords(clickState.oldStone);
}

function moveToNewPos() {
	updateCoords(clickState.nearPos);
}

function updateCoords(to) {
	clickState.currentStone.x = to.x;
	clickState.currentStone.y = to.y;
}

function freeOldPos(stone) {
	for (var i = 0, len = gameState.positions.length; i < len; i++) {
		var pos = gameState.positions[i];
		if (pos.contains(stone.x, stone.y) && pos.occupied()) {
			pos.cargo = null;
		}
	}
}

function pushNewStone() {
	if (clickState.currentStone.firstMove) {
		clickState.currentStone.firstMove = false;
		if (++gameState.currentPlayer.stonesPlaced < nrOfStones) {
			gameState.currentPlayer.addStone(new Stone(newStoneX, newStoneY, newStoneR));
		}
	}
}

function neighborPos(stone) {
	var neighX = gameState.positions.filter(pos => (stone.x == pos.x) && (stone.y != pos.y));
	var nearestX = neighX.sort((posA, posB) => distance(posA, stone) - distance(posB, stone))[0];
	neighX = neighX.filter(pos => distance(pos, stone) == distance(nearestX, stone));

	var neighY = gameState.positions.filter(pos => (stone.y == pos.y) && (stone.x != pos.x));
	var nearestY = neighY.sort((posA, posB) => distance(posA, stone) - distance(posB, stone))[0];
	neighY = neighY.filter(pos => distance(pos, stone) == distance(nearestY, stone));

	return [].concat(neighX, neighY);
}

function inNeighborPos(e) {
	return neighborPos(clickState.oldStone).map(pos => pos.contains(e.pageX, e.pageY))
								.reduce((pV, cV, cI, arr) => pV || cV);
}

function allBoardStonesInMill() {
	var onlyStonesOnBoard = gameState.currentPlayer.stones.filter(stone => !stone.firstMove);
	if (onlyStonesOnBoard.length == 0) return false;
	return onlyStonesOnBoard.map(stone => stone.isInMill())
							.reduce((pV, cV, cI, arr) => pV && cV);
}

function isAcrossCenter(stone, pos) {
	var center = startpos + unit * 3;
	if (!((stone.x == center && pos.x == center) || (stone.y == center && pos.y == center))) return false;
	return posIsBetween(stone, {x: center, y: center}, pos);
}

function isBetween(a, b, c) {
	return (a < b && b < c) || (c < b && b < a);
}

function posIsBetween(a, b, c) {
	return (a.x == c.x && isBetween(a.y, b.y, c.y)) ||
		   (a.y == c.y && isBetween(a.x, b.x, c.x));
}

function distance (a, b) {
	return Math.sqrt((a.x - b.x) * (a.x - b.x) +
					 (a.y - b.y) * (a.y - b.y));
}

function equalPosition(a, b) {
	return a.x == b.x && a.y == b.y;
}

function sameColor(a, b) {
	return a.fill == b.fill;
}

function upNearUnoccupiedPos(e) {
	for (var i = 0, len = gameState.positions.length; i < len; i++) {
		var pos = gameState.positions[i];
		if (pos.contains(e.pageX, e.pageY) && !pos.occupied()) {
			clickState.nearPos = pos;
			return true;
		}
	}
	return false;
}

function clickInStone(e) {
	for (var i = 0, len = gameState.currentPlayer.stones.length; i < len; i++) {
		var stone = gameState.currentPlayer.stones[i];
		if (stone.contains(e.pageX, e.pageY)) {
			clickState.currentStone = stone;
			return true;
		}
	}
	return false;
}

function initPositions() {

	var posCoords = [{x: 0, y: 0}, {x: 3, y: 0}, {x: 6, y: 0},
					 {x: 1, y: 1}, {x: 3, y: 1}, {x: 5, y: 1},
					 {x: 2, y: 2}, {x: 3, y: 2}, {x: 4, y: 2},
					 {x: 0, y: 3}, {x: 1, y: 3}, {x: 2, y: 3},
					 {x: 4, y: 3}, {x: 5, y: 3}, {x: 6, y: 3},
					 {x: 2, y: 4}, {x: 3, y: 4}, {x: 4, y: 4},
					 {x: 1, y: 5}, {x: 3, y: 5}, {x: 5, y: 5},
					 {x: 0, y: 6}, {x: 3, y: 6}, {x: 6, y: 6}];

	for (var i = 0, len = posCoords.length; i < len; i++) {
		var x = posCoords[i].x;
		var y = posCoords[i].y;
		gameState.positions.push(new Position(startpos + unit * x, startpos + unit * y));
	}
}

function initPlayers() {
	var playerWhite = new Player('white');
	playerWhite.addStone(new Stone(newStoneX, newStoneY, newStoneR));
	gameState.players.push(playerWhite);

	var playerBlack = new Player('black');
	playerBlack.addStone(new Stone(newStoneX, newStoneY, newStoneR));
	gameState.players.push(playerBlack);

	gameState.currentPlayer = gameState.players[0];
}

function nextPlayerToMove() {
	var currentPlayerIndex = gameState.players.findIndex(player => player === gameState.currentPlayer);
	currentPlayerIndex = ++currentPlayerIndex % gameState.players.length;
	gameState.currentPlayer = gameState.players[currentPlayerIndex];
}

function drawBoard() {

	rect(startpos + unit * 0, startpos + unit * 0, unit * 6, unit * 6); // x, y, w, h
	rect(startpos + unit * 1, startpos + unit * 1, unit * 4, unit * 4);
	rect(startpos + unit * 2, startpos + unit * 2, unit * 2, unit * 2);

	line(startpos + unit * 3, startpos + unit * 0, startpos + unit * 3, startpos + unit * 2); // x1, y1, x2, y2
	line(startpos + unit * 6, startpos + unit * 3, startpos + unit * 4, startpos + unit * 3);
	line(startpos + unit * 3, startpos + unit * 6, startpos + unit * 3, startpos + unit * 4);
	line(startpos + unit * 0, startpos + unit * 3, startpos + unit * 2, startpos + unit * 3);

	for (var i = 0, len = gameState.positions.length; i < len; i++) {
		gameState.positions[i].draw();
	}
}

function drawPlayerStones() {
	for (var i = 0, len = gameState.players.length; i < len; i++) {
		if(gameState.players[i] !== gameState.currentPlayer) gameState.players[i].drawStones();
	}
	gameState.currentPlayer.drawStones();
}

function drawRemainingNr() {
	var temp = newStoneY;
	for (var i = 0, len = gameState.players.length; i < len; i++) {
		ctx.strokeText('(' + (nrOfStones - gameState.players[i].stonesPlaced) + ')', newStoneX + 50, newStoneY + 10);
		newStoneY += 100;
	}
	newStoneY = temp;
}

function filledCircle(x, y, r, fill) {
	ctx.beginPath();
    ctx.arc(x, y, r, 0, 2 * Math.PI);
    ctx.fillStyle = fill;
    ctx.fill();
}

function emptyCircle(x, y, r, stroke) {
	ctx.beginPath();
    ctx.arc(x, y, r, 0, 2 * Math.PI);
    ctx.strokeStyle = stroke;
    ctx.stroke();
    ctx.strokeStyle = 'black';
}

function rect(x, y, w, h) {
	ctx.beginPath();
	ctx.rect(x,y,w,h);
	ctx.closePath();
	ctx.stroke();
}

function line(x1, y1, x2, y2) {
	ctx.beginPath();
	ctx.moveTo(x1, y1);
	ctx.lineTo(x2, y2);
	ctx.closePath();
	ctx.stroke();
}

function logArr (arr) {
	console.log('arr:');
	for (var i = 0, len = arr.length; i < len; i++) {
		console.log(arr[i]);
	}
}
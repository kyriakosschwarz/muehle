

function init() {
	initPlayers();
}

function Player(color) {
	this.color = color;
	this.stones = [];
	this.stonesPlaced = 0;
}

Player.prototype.addStone = function(stone) {
	stone.fill = this.color;
	this.stones.push(stone);
	// console.log('blah');
};

Player.prototype.removeStone = function(index) {
	this.stones.splice(index, 1);
};

Player.prototype.drawStones = function() {
	for (var i = 0, len = this.stones.length; i < len; i++) {
		this.stones[i].draw();
	}
};

function Stone(x, y, r) {
	this.x = x;
	this.y = y;
	this.r = r;
	this.fill = 'undefined color';
	this.z = 0;
	this.firstMove = true;
}

Stone.prototype.draw = function() {
  	filledCircle(this.x, this.y, this.r, this.fill);
	emptyCircle(this.x, this.y, this.r, 'black');
};

Stone.prototype.contains = function(mx, my) {
	return distance(this, {x: mx, y: my}) < this.r;
};

Stone.prototype.isWhite = function () {
	return this.fill == 'white';
};

Stone.prototype.isInTriple = function() {
	var hor = 0, ver = 0;
	for (var i = 0, len = positions.length; i < len; i++) {
		var pos = positions[i];
		if (pos.occupied() && sameColor(this, pos.cargo) && !isAcrossCenter(this, pos)) {
			if (this.y == pos.y) hor++;
			if (this.x == pos.x) ver++;
		}
	}
	return hor >= 3 || ver >= 3;
};

function Position (x, y) {
	this.x = x;
	this.y = y;
	this.name = x + ',' + y;
	this.cargo = null; 			// holds a stone
}

Position.prototype.draw = function () {
	filledCircle(this.x, this.y, 5, '#333');
};

Position.prototype.occupied = function() {
	return this.cargo !== null;
};

Position.prototype.contains = function(mx, my) {
	return distance(this, {x: mx, y: my}) < nearPosDist;
};

function initPlayers() {
	
	var p = new Player('white');
	var s = new Stone(1,2,3);
	p.addStone(s);
	players.push(p);

	console.log(players[0]);

	// var playerBlack = new Player('black');
	// playerBlack.addStone(new Stone(newStoneX, newStoneY + 100, newStoneR));
	// players.push(playerBlack);
}

players = [];
init();